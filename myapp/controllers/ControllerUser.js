
/*
 * GET users listing.
 */





exports.index = function(req,res){


  db.User.find().exec(function(error,users){

    if (error) return res.json(error);

    res.render('user/index.html', {
      title: 'Usuarios',
      users: users

    });
  });
};

exports.nuevo = function(req,res){
  res.render('user/nuevo.html',{title: "Nuevo Usuario"});
}

exports.newUser = function(req,res){
  var u = req.body;

  // podemos acceder a DB sin hacer
  // require porque es global
  var newUser = new db.User({
    nombre: u.nombre,
    apellido: u.apellido,
    telefono: u.telefono,
    direccion: u.direccion,
    isActive: u.isActive === 'on' ? true : false
  });

  newUser.save(function(error, user) {

    if (error) response.json(error);

    res.redirect('/');

  });
}
